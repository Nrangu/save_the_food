﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Base
{
    /// <summary>
    /// Базовый класс для 2D объектов
    /// Отличие от BaseSceneObject - доступ к spriteRenderer
    /// </summary>
    public abstract class Base2DSceneObject : BaseSceneObject
    {
        #region protected fields
        protected SpriteRenderer _spriteRenderer;
        protected Sprite _spriteImage;
        protected Color _spriteColor;
        #endregion

        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            if (GetComponent<SpriteRenderer>())
            {
                _spriteRenderer = GetComponent<SpriteRenderer>();
                _spriteImage = _spriteRenderer.sprite;
                _spriteColor = _spriteRenderer.color;
            }
        }
        #endregion

        #region public properties
        public SpriteRenderer SpriteRender
        {
            get { return _spriteRenderer; }
        }

        public Sprite Image
        {
            get { return _spriteImage; }
            set
            {
                _spriteImage = value;
                
            }
        }
        public Color SpriteColor
        {
            get { return _spriteColor; }
            set
            {
                _spriteColor = value;
                if (_spriteRenderer)
                {
                    _spriteRenderer.color = value;
                }
            }
        }
        #endregion
        #region protected properties
        public Sprite SpriteImage
        {
            get { return _spriteRenderer ?  null :  _spriteRenderer.sprite; }
            set
            {
                _spriteImage = value;
                if (_spriteRenderer)
                {
                    _spriteRenderer.sprite = value;
                }
            }
        }

        #endregion

    }
}

