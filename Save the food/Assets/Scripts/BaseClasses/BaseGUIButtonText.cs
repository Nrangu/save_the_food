﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;

namespace SaveTheFood
{
    public class BaseGUIButtonText : BaseGUIText
    {
        #region private fields
        #endregion
        #region protected methods
        void Awake()
        {
            
            foreach (Transform elem in transform)
            {
                Text tmp = elem.GetComponent<Text>();
                if (tmp)
                {
                    TextComponent = tmp;
                }
            }
           
            if (TextComponent)
            {
                TextLeft = "left";
                TextRight = "Right";
            }
        }
        #endregion

    }
}