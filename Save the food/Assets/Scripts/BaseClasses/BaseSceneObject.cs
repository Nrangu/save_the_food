﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base
{
    /// <summary>
    /// Базовый класс всех объектов на сцене
    /// </summary>

    public abstract class BaseSceneObject : MonoBehaviour
    {
        #region protected fields
        protected int _layer;
        protected string _name;
        protected Renderer _renderer;
        protected GameObject _instanceObject;
        protected Transform _objectTransform;
        protected Vector3 _position;
        #endregion
        #region protected virtual methods
        protected virtual void Awake()
        {
            _instanceObject = gameObject;
            _name = _instanceObject.name;
            _objectTransform = _instanceObject.transform;
            if (GetComponent<Renderer>())
            {
                _renderer = GetComponent<Renderer>();
            }

        }
        #endregion
        #region private methods
        void SetLayer( Transform obj, int value)
        {
            obj.gameObject.layer = value;
            if (obj.childCount > 0)
            {
                foreach (Transform elem in obj)
                {
                    SetLayer(elem, value);
                }
            }
        }
        #endregion
        #region Propeties
        public GameObject InstanceObject
        {
            get { return _instanceObject; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; _instanceObject.name = value; }
        }
        public Transform GetTransform
        {
            get { return _objectTransform; }

        }
        public Vector3 Position
        {
            get
            {

                if (InstanceObject != null)
                {
                    _position = GetTransform.position;
                }
                return _position;
            }
            set
            {
                _position = value;

                if (InstanceObject != null)
                {
                    GetTransform.position = _position;
                }
            }
        }
        public int Layer {
            get { return _layer; }
            set
            {
                _layer = value;
                if (_instanceObject != null)
                {
                    _instanceObject.layer = value;
                    SetLayer(GetTransform, value);
                }
            }
        }
        public Renderer Render
        {
            get { return _renderer; }
        }
        #endregion
    }

}
