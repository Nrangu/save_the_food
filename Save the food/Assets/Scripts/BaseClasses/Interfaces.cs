﻿namespace Base.Interfaces
{
    enum States
    {
        EMPTY, MISS, DESTROYED
    }

    /// <summary>
    /// Базовый интерфейс для всех состояний
    /// Дает возможность активировать/деактивировать состояние
    /// А так же узнать актино сосотояние или нет
    /// </summary>
    public interface IBaseState
    {
        bool Active {
            get;
        }
        void On();
        void Off();

        // Что бы реакция на триггеры была только в одном месте
        // предположим в контроллере состояний.
        // Описываем функции которые будут вызывать при срабатываниии триггера
        void onMouseDown(); //
        void onMouseDrag();
        void onMouseUp();
    }
}