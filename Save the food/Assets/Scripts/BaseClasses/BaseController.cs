﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base
{
    public abstract class  BaseController: MonoBehaviour
    {
        #region private fields
        private bool _enabled = false; // скорее всего нужно другое имя так как у скрипта есть enabled и лего спутать имена
        #endregion
        #region public properties
        public bool Enabled
        {
            get { return _enabled; }
            private set { _enabled = value; }
            
                 
        }
        #endregion
        #region public methods
        public virtual void On()
        {
            Enabled = true;
        }
        public virtual void Off()
        {
            Enabled = false;
        }
        #endregion
    }
}