﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Interfaces;

namespace Base
{
    /// <summary>
    /// Базовый класс для управлением состояния объекта
    /// </summary>
    public abstract class BaseState : BaseSceneObject, IBaseState
    {
        #region protected fields
        protected bool _isActive = false;
        #endregion
        #region properties
        public bool Active
        {
            get { return _isActive; }
            private set { _isActive = value; }
        }
        #endregion

        #region protected methods
        // Этот метод нужено использовать для обновления 
        // Почему не стандартный Update?
        // Так как состояние может быть активно либо не активно
        // что бы избавить от лишней проверки в классе наследнике
        // стандатрный Update прячем, а в ней вызываем метод для обновления
        // А в стандартном Update проверяем активно ли состояние
        protected virtual void UpdateState() { }
        protected virtual void MouseDown() { }
        protected virtual void MouseDrag() { }
        protected virtual void MouseUp() { }
        #endregion
        #region pritate methods
        private void Update()
        {
            if (!Active) return;
            UpdateState();
        }
        #endregion

        #region public methods
        public virtual void On()
        {
            Active = true;
        }
        public virtual void Off()
        {
            Active = false;
        }
        // Что бы не писать обработку события напрямую, воизбежания не поняток
        // сделан переопределяемый метод который и будет вызываться в контроллере состояния
        // при нажатии клавиши мыши. Таким образом контроль всех нажатий будет в контроллере
        public void onMouseDown()
        {
            if (!Active) return;
            MouseDown();
        }
        public void onMouseDrag()
        {
            if (!Active) return;
            MouseDrag();

        }
        public void onMouseUp()
        {
            if (!Active) return;
            MouseUp();
        }
        #endregion

    }
}