﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Interfaces;

namespace Base
{
    //_currentState - все присвоения сделать через свойства
    public abstract class BaseStatesController : BaseController {


        #region protected fields
        protected int _countStates = 0;
        protected int _currentState;
        protected GameObject _instanceObject;
        protected IBaseState[] _states;
        #endregion
        #region private methods
        //  так как контроллер может быть выключен
        // а это можно забыть проверить.  Закрываем Update, и делаем виртуальный метод для обновления
        // тогда в классе наслденике переопределям виртуальный метод и вызываться он будет только тогда когда контроллер активен
        // это же нужно сделать и для других методов для которых важно активен контроллер или нет
        // Скорее всего это все можно сделать намного лучше и удобнее, но я пока не знаю как
        private void Update()
        {
            if (!Enabled) return;
            UpdateController();
        }
        private void OnMouseDown()
        {
            if (!Enabled) return;
            MouseDown();
        }
        private void OnMouseDrag()
        {
            if (!Enabled) return;
            MouseDrag();
        }
        private void OnMouseUp()
        {
            if (!Enabled) return;
            MouseUp();
        }
        #endregion
        #region protected methods
        protected virtual void UpdateController() { }
        protected virtual void MouseDown() { }
        protected virtual void MouseDrag() { }
        protected virtual void MouseUp() { }

        
        protected virtual void Awake()
        {
            _instanceObject = gameObject;
            _states = new IBaseState[_countStates];
        }

        #endregion
        #region public methods
        // Казалось бы методы Setstate  SwitchState одинаковые
        // Но первый приватный, для того что бы можно было установить состояние миную проверку активности
        // например при старте. У объекта должно быть активым какое либо состояние
        // SwitchState позволяет переключать состояние только если контроллер активен
        // Если придумаю другой способ изменю

        public void SwitchState(int state)
        {
            if (!Enabled) return;
            SetState(state);
        }
        public override void On()
        {
            base.On();
            SetState(_currentState);
        }
        #endregion
        #region private methods
        private bool SetState(int state)
        {
            if (state >= _states.Length || state < 0) return false;
            if (_states[state].Active)
            {
                _currentState = state;
                return true;
            }
            if (_states[_currentState].Active)
            {
                _states[_currentState].Off();
            }
            _states[state].On();
            _currentState = state;
            return true;

        }
        #endregion
        #region public properties
        public GameObject InstanceObject
        {
            get { return _instanceObject; }
        }
        public int CurrentState
        {
            get { return _currentState; }

            private set { _currentState = value; }
        }
        #endregion
    }
}