﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;

namespace Base
{
    public class BaseGUIText : MonoBehaviour
    {

        #region private fields
        private string _textLeft ;
        private string _textRight;
        private Text _textComponent = null;
        #endregion
        #region public properties
        public string TextLeft
        {
            get { return _textLeft; }
            set
            {
                _textLeft = value;
                if (_textComponent)
                {
                    _textComponent.text = _textLeft + _textRight;
                }
            }
        }
        public string TextRight
        {
            get { return _textRight; }
            set
            {
                _textRight = value;
                if (_textComponent)
                {
                    _textComponent.text = _textLeft + _textRight;
                }
            }
        }
        public Text TextComponent
        {
            get { return _textComponent; }
            set { _textComponent = value; }
        }
        #endregion
    }

}
