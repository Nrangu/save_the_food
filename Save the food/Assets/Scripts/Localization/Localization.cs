﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Localization
{
    // Класс для работы с локализациями

//переделать используя enum
// сейчас можно не правильно написать язык

//Возможно нужно сделать текущий словарь
//и присваивать ему значение при выборе языка
    static class Localizations
    {
        #region private fields
        private static string _currentLanguage = "ENGLISH";
        private static Dictionary<string, Dictionary<string, string>> _texts = new Dictionary<string, Dictionary<string, string>>();
        #endregion
        #region private methods
        static Localizations()
        {
            
            Dictionary<string, string> tmp = new Dictionary<string, string>();
            tmp.Add("START", "Старт");
            tmp.Add("MAINMENU", "Основное меню");
            tmp.Add("RESTART", "Перезапустить");
            tmp.Add("RETURN", "Возврат");
            tmp.Add("EXIT", "Выход");
            tmp.Add("SCORE", "Очки");
            tmp.Add("ENEMIES", "Враги");
            tmp.Add("YOULOSE", "Вы проиграли !!!!");
            tmp.Add("TIME", "Время :");

            _texts.Add( "RUSSIAN", tmp ) ;

            tmp = new Dictionary<string, string>();
            tmp.Add("START", "Start ");
            tmp.Add("MAINMENU", "Main menu");
            tmp.Add("RESTART", "Restart");
            tmp.Add("RETURN", "Return");
            tmp.Add("EXIT", "Exit");
            tmp.Add("SCORE", "Score");
            tmp.Add("ENEMIES", "Enemies");
            tmp.Add("YOULOSE", "You lose !!!!!");
            tmp.Add("TIME", "Time :");

            _texts.Add("ENGLISH", tmp);
        }
        #endregion
        #region public 
        public static string CurrentLanguage
        {
            get { return _currentLanguage; }
            set { _currentLanguage = value.ToUpper(); }
        }
        public static string GetText(string key)
        {
            key = key.ToUpper();
            if (_texts[_currentLanguage].ContainsKey(key))
            {
                return _texts[_currentLanguage][key];
            }
            return "";
        }
        #endregion
    }
}
