﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;

namespace Localization
{
    enum Side { LEFT, RIGHT};
    public class GUILocalizationText : MonoBehaviour
    {
        #region private fields
        [SerializeField] private string _key;
        [SerializeField] private string _dafaultText;
        [SerializeField] private Side _side = Side.LEFT;
        private  BaseGUIText _text;
        #endregion
        #region private methods
        private void Start()
        {

            Text tmp = GetComponent<Text>();

            if (!tmp)
            {
                foreach( Transform elem in gameObject.transform)
                {
                    tmp = elem.gameObject.GetComponent<Text>();
                    if (tmp) break;
                }
            }

            _text = GetComponent<BaseGUIText>();
            if (!_text)
            {
                _text = gameObject.AddComponent<BaseGUIText>();
            }

            _text.TextComponent = tmp;

            UpdateText();

        }
        #endregion
        #region public
        public void UpdateText()
        {

            if (_side == Side.LEFT)
            {
                _text.TextLeft = Localizations.GetText(_key);
            }
            else
            {
                _text.TextRight = Localizations.GetText(_key);

            }
        }
        #endregion
    }
}