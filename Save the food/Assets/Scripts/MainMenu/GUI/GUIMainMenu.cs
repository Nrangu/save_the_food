﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Base;
namespace SaveTheFood
{

    public class GUIMainMenu : BaseGUI
    {
        #region pivate fields
        GUISettings _settingsMenu;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            _settingsMenu = Resources.FindObjectsOfTypeAll<GUISettings>()[0];

        }
        #endregion
        #region buttons clicks
        public void StartClick()
        {
            SceneManager.LoadScene("Game");
        }
        public void SettingsClick()
        {
            _settingsMenu.ActiveObject = true;
        }
        public void ExitClick()
        {
            Application.Quit();
        }
        #endregion
    }
}