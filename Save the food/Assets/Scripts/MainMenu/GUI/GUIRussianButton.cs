﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Localization;
using Base;

namespace SaveTheFood
{

    public class GUIRussianButton : MonoBehaviour
    {
        public void RussianClick()
        {
            Localizations.CurrentLanguage = "Russian";
            foreach (GUILocalizationText elem in Resources.FindObjectsOfTypeAll<GUILocalizationText>())
            {
                elem.UpdateText();
            }
        }
    }
}
