﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Localization;


namespace SaveTheFood
{
    public class GUIEnglishButton : MonoBehaviour
    {
        public void EnglishClick()
        {
            Localizations.CurrentLanguage = "English";
            foreach (GUILocalizationText elem in Resources.FindObjectsOfTypeAll<GUILocalizationText>())
            {
                elem.UpdateText();
            }

        }
    }
}