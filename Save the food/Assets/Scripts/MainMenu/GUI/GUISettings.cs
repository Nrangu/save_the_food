﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

public class GUISettings : BaseGUI {
    protected override void Awake()
    {
        base.Awake();

    }
    #region buttons clicks
    public void CancelClick()
    {
        ActiveObject = false;
    }
    public void OkClick()
    {
        ActiveObject = false;
    }
    #endregion

}
