﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Base;

namespace SaveTheFood
{
    public class GUIStartButton : BaseGUI
    {
        #region private fields;
        #endregion
        #region protected methods
        #endregion
        #region private methods
        #endregion
        #region public methods
        public void StartClick()
        {
            SceneManager.LoadScene("Game");

        }
        #endregion
    }
}