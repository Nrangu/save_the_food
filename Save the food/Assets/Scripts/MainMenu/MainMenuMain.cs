﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Localization;

namespace SaveTheFood
{


    public class MainMenuMain : MonoBehaviour
    {
        #region private fields
        private GUIStartButton _startButton;


        bool flag = false;
        #endregion
        #region private methods
        // Use this for initialization
        void Start()
        {
            if (Instance)
            {
                Object.Destroy(gameObject);
            }

            Instance = this;
            _startButton = Resources.FindObjectsOfTypeAll<GUIStartButton>()[0];
        }

        private void Update()
        {
            Debug.Log(Time.deltaTime);
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            if (!flag)
            {
                flag = true;
                _startButton.ActiveObject = true;
            }
        }
        #endregion

        #region public properties
        public static MainMenuMain Instance { get; private set; }
        #endregion
    }
}