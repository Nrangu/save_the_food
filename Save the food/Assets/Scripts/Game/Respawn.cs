﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
namespace SaveTheFood
{
    public class Respawn : BaseSceneObject
    {
  //      [SerializeField] private GameObject _gameObject;
//
        public void Create(GameObject _gameObject)
        {
            _gameObject.transform.position = new Vector3( Position.x, Position.y, _gameObject.transform.position.z);
            GameObject _tmpObject = Instantiate(_gameObject) as GameObject;
            (_tmpObject.GetComponent<CockroachController>()).On();

        }
    }
}