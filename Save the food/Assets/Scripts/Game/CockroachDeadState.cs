﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace SaveTheFood
{
    public class CockroachDeadState : BaseState2D
    {
        #region private fields
        private float _deltaTime = 0;
        private float _time = 3f;
        #endregion
        #region public methods
        public override void On()
        {
            base.On();
            SpriteImage = Image;
            Position = new Vector3(Position.x, Position.y, Position.z + 1);

            //    StartCoroutine(Destroy());
        }
        #endregion
        #region private methods
        IEnumerator Destroy()
        {
            yield return new WaitForSeconds(1);
            Object.Destroy(InstanceObject);

        }
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
        }
        protected override void UpdateState()
        {
            base.UpdateState();
            if (_deltaTime >= _time)
            {
                Object.Destroy(InstanceObject);
            }
            _deltaTime += Time.deltaTime;
        }
        #endregion
    }
}