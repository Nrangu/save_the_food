﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace SaveTheFood
{

    public class CockroachEatState : BaseState2D
    {
        #region private fields
        private Target _food;
        private float _damage = 1;
        private float _deltaTime = 0;
        private float _time = 0.5f;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            // это присвоение позволяет таракану нанести урон только добежав до цели
            base.Awake();
            _deltaTime = _time;
        }
        protected override void UpdateState()
        {
            if (GameSceneMain.Pause) return;
            base.UpdateState();
            _deltaTime += Time.deltaTime;

            if (_deltaTime >= _time)
            {

                _food.Hit(_damage);
                _deltaTime = 0;
            }
        }
        #endregion
        #region public properties
        public Target Target
        {
            get { return _food; }
            set { _food = value; }
        }
        #endregion
        #region private methods
        #endregion
    }
}