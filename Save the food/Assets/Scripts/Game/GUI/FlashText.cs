﻿using System.Collections;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;

namespace SaveTheFood
{
    public class FlashText : MonoBehaviour
    {

        [SerializeField] private float _time = 1.5f;
        private float _deltaTime = 0;
        private float _step = 0;
        private Image _image;
        private Color _color;
        private bool _flag = false;
        private void Awake()
        {
            _image = GetComponent<Image>();
            _step = 1f / _time;
        }

        private void Start()
        {
        }
        private void Update()
        {
            _color = _image.color;
            _color.a = _deltaTime * _step;
            _image.color = _color;

           if (!_flag)
            {
                _deltaTime += Time.deltaTime;
            }else
            {
                _deltaTime -= Time.deltaTime;

            }

            if (_deltaTime > _time)
            {
                _flag = true;
            }
            if (_deltaTime < 0)
            {
                _flag = false;
            }
        }
    }
}