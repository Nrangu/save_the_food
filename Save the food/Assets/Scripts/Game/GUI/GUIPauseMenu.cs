﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Base;

namespace SaveTheFood
{
    public class GUIPauseMenu : BaseGUI
    {
        #region private fields
        private GUIPauseButton _pauseButton;
        private GUISettings _settingsMenu;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            _pauseButton = (Resources.FindObjectsOfTypeAll<GUIPauseButton>())[0];
        }
        #endregion
        #region public methods buttons click
        public void ReturnClick()
        {
            _pauseButton.ActiveObject = true;
            GameSceneMain.Pause =false;
            //Time.timeScale = 1;
            ActiveObject = false;
        }
        public void SettingsClick()
        {
            _settingsMenu.ActiveObject = true;
        }
        public void RestartClick()
        {
            //            Scene loadedLevel = ;
            //Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        public void ExtiClick()
        {
            Application.Quit();
        }
        public void MainMenuClick()
        {
            //Time.timeScale = 1;
            GameSceneMain.Pause = false;
            SceneManager.LoadScene("MainMenu");
        }
        #endregion
    }
}