﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;
using Localization;

namespace SaveTheFood
{
    public class GUIScore : BaseGUI
    {
        #region private fields
        [SerializeField] private string _leftText;
        [SerializeField] private string _rightText;
        private BaseGUIText _GUIText;
        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            _GUIText = GetComponent<BaseGUIText>();
            if (!_GUIText)
            {
                _GUIText = InstanceObject.AddComponent<BaseGUIText>();
            }

            Text tmp = GetComponent<Text>();

            if (tmp)
            {
                _GUIText.TextComponent = tmp;
            }

            if (!GetComponent<GUILocalizationText>())
            {
                _GUIText.TextRight = _rightText;
            }
            _GUIText.TextLeft = _leftText;
        }
        #endregion
        #region public methods
        public void ScoreUp(string _score)
        {
            _GUIText.TextRight = " : " + _score;
        }
        #endregion

    }
}