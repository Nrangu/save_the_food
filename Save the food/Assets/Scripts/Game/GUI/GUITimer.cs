﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base;
using Localization;

namespace SaveTheFood
{

    public class GUITimer : BaseGUI { 
    #region private fields
    [SerializeField] private string _leftText;
    [SerializeField] private string _rightText;
    private BaseGUIText _GUIText;
        float _time = 0;
    #endregion
    #region protected methods
    protected override void Awake()
    {
        base.Awake();
            ActiveObject = true;
            _GUIText = GetComponent<BaseGUIText>();
        if (!_GUIText)
        {
            _GUIText = InstanceObject.AddComponent<BaseGUIText>();
        }

        Text tmp = GetComponent<Text>();

        if (tmp)
        {
            _GUIText.TextComponent = tmp;
        }

        if (!GetComponent<GUILocalizationText>())
        {
            _GUIText.TextRight = _rightText;
        }
        _GUIText.TextLeft = _leftText;
    }

        #endregion
        #region private methdos
        private void Update()
        {
            _time += Time.deltaTime;
            _GUIText.TextRight = " " + _time.ToString("N2"); 
        }
        #endregion
        #region public methods
        #endregion

    }
}