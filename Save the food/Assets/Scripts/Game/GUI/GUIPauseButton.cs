﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace SaveTheFood
{
    public class GUIPauseButton : BaseGUI
    {
        #region private fields
        GUIPauseMenu _pauseMenu;
        #endregion
        #region public methods
        public void PauseClick()
        {
            GameSceneMain.Pause = true; ;
            _pauseMenu.ActiveObject = true;
            ActiveObject = false;
        }

        #endregion
        #region protected methods
        protected override void Awake()
        {
            base.Awake();
            _pauseMenu = Resources.FindObjectsOfTypeAll<GUIPauseMenu>()[0];
        }
        #endregion
    }
}