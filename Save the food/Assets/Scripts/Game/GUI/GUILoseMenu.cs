﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Base;

namespace SaveTheFood
{

    public class GUILoseMenu : BaseGUI
    {
        #region public methods
        public void RestartClick()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }
        public void ManiMenuClick()
        {
            GameSceneMain.Pause = false;
            SceneManager.LoadScene("MainMenu");
        }

        public void ExitClick()
        {
            Application.Quit();
        }
        #endregion
    }
}