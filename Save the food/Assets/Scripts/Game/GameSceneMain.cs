﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
using Localization;

namespace SaveTheFood
{
    //     Enemy type  
    //     cockroach type
    //     cockroach count

    // food type
    // array  cockroachs types 
    // структура описывающая врага.
    // _enemy задает  тип врага 
    // _frequency задачает частоту появления врага, тоесть что бы
    // описать что каждый пятый враг будет данного типа нужно задать значение 5
    public struct Enemy
    {
        public GameObject _enemy; // тип
        public int _frequency;
    }
    public struct Level
    {
        public GameObject _food;
        public Enemy[] _enemies;
        public int _countEnemies;
        public float timeStart;
        public float timeFinish;
        public float[] timeStarts;
        public int[] points;
        

    }


    public class GameSceneMain : MonoBehaviour
    {
        #region private fields
        Level[] _levels;
        float _timer;
        
        //        [SerializeField] private Respawn _respawnPoint;
        [SerializeField] Respawn[] _respawnPoints;
        private float _time = 0;
        //private bool _pause = false;
        private static GameSceneMain _instance;
        private int _score = 0;
        private GUIScore _countScore;
        private GUICountEnemies _textCountEnemies;
        private Target _food;
        private bool _tmpFlag = false;
        private GUILoseMenu _loseMenu;
        [SerializeField] private float _timeInstance = 0;
        [SerializeField] private Transform _foodPosition;

        private int _countWaves = 0;
        private int _currentWave = 0;
        private int _countEnemies = 0;
        private int _currentEnemy = 0;
        private int _currentLevel = 0;
        private int _countEnemiesDestroed = 0;
        private Enemy[] _frequency = null;
        private GameObject _currentFood = null;

        private static bool _pause = false;
        private float[] _timeInstanceEnemies = null;
        private int respP1 = 0;
        private int respP2 = 0;
        private HealthLine _healthLine;
        [SerializeField]  Respawn[] RPoints;
        private int _respawnIndex = 0;



        #endregion
        #region private methods
        private void Start()
        {
            
            Pause = false;

            if (_instance)
            {
                Object.Destroy(this);
                return;
            }

            _instance = this;

            //_respawnPoints = FindObjectsOfType<Respawn>();
            GUIPauseButton _tmp = (Resources.FindObjectsOfTypeAll<GUIPauseButton>()[0]);
            _countScore = (Resources.FindObjectsOfTypeAll<GUIScore>())[0];
            _tmp.ActiveObject = true;
            _countScore.ActiveObject = true;
            _textCountEnemies = (Resources.FindObjectsOfTypeAll<GUICountEnemies>())[0];
            _textCountEnemies.ActiveObject = true;
            _food = FindObjectOfType<Target>();
            _loseMenu = Resources.FindObjectsOfTypeAll<GUILoseMenu>()[0];




            _time = _timeInstance;
            _textCountEnemies.CountEnemies(_countEnemies.ToString(), (_countEnemies - _currentEnemy).ToString());
            _countScore.ScoreUp(_score.ToString());
            _currentLevel = 0;
            _countWaves = 1;
            _currentWave = 1;

            LevelInit(_currentLevel);





        }
        private void Awake()
        {
            _healthLine = FindObjectOfType<HealthLine>();
            _timeInstanceEnemies = new float[] { 0, 0.5f,  1f, 1.5f, 0.5f,1f, 0.5f, 1f, 1.5f};

            // нужно проверять все на ошибки
            // нужно придумать логирование - статический класс
            GameObject _Cockroach = Resources.Load("Enemies/Cockroach", typeof(GameObject)) as GameObject;
            GameObject _FatCockroach = Resources.Load("Enemies/FatCockroach", typeof(GameObject)) as GameObject;
            GameObject _RunnerCockroach = Resources.Load("Enemies/RunnerCockroach", typeof(GameObject)) as GameObject;
            _levels = new Level[15];

            // При описании уровня задавая врагов, в нулевой элемент массива _levels[]._enemies
            // помещаем значение врага по умолчанию и частоту появления лучше поставить 0

            _levels[0]._food = Resources.Load("Food/Cracker", typeof(GameObject)) as GameObject;
            _levels[0]._countEnemies = 20;
            _levels[0]._enemies = new Enemy[1];
            _levels[0]._enemies[0]._enemy = _Cockroach;
            _levels[0]._enemies[0]._frequency = 0;
            _levels[0].timeStart = 1.5f;
            _levels[0].timeFinish = 0.5f;


            _levels[1]._food = Resources.Load("Food/Cake", typeof(GameObject)) as GameObject;
            _levels[1]._countEnemies = 30;
            _levels[1]._enemies = new Enemy[2];
            _levels[1]._enemies[0]._enemy = _Cockroach;
            _levels[1]._enemies[0]._frequency = 0;
            _levels[1]._enemies[1]._enemy = _FatCockroach;
            _levels[1]._enemies[1]._frequency = 10;
            _levels[1].timeStart = 1.5f;
            _levels[1].timeFinish = 0.5f;

            _levels[2]._food = Resources.Load("Food/Donut", typeof(GameObject)) as GameObject;
            _levels[2]._countEnemies = 40;
            _levels[2]._enemies = new Enemy[2];
            _levels[2]._enemies[0]._enemy = _Cockroach;
            _levels[2]._enemies[0]._frequency = 0;
            _levels[2]._enemies[1]._enemy = _FatCockroach;
            _levels[2]._enemies[1]._frequency = 8;
            _levels[2].timeStart = 1f;
            _levels[2].timeFinish = 0.5f;

            _levels[3]._food = Resources.Load("Food/Cheesecake", typeof(GameObject)) as GameObject;
            _levels[3]._countEnemies = 50;
            _levels[3]._enemies = new Enemy[2];
            _levels[3]._enemies[0]._enemy = _Cockroach;
            _levels[3]._enemies[0]._frequency = 0;
            _levels[3]._enemies[1]._enemy = _FatCockroach;
            _levels[3]._enemies[1]._frequency = 7;
            _levels[3].timeStart = 0.9f;
            _levels[3].timeFinish = 0.25f;

            _levels[4]._food = Resources.Load("Food/Pizza", typeof(GameObject)) as GameObject;
            _levels[4]._countEnemies = 60;
            _levels[4]._enemies = new Enemy[3];
            _levels[4]._enemies[0]._enemy = _Cockroach;
            _levels[4]._enemies[0]._frequency = 0;
            _levels[4]._enemies[1]._enemy = _FatCockroach;
            _levels[4]._enemies[1]._frequency = 5;
            _levels[4]._enemies[2]._enemy = _RunnerCockroach;
            _levels[4]._enemies[2]._frequency = 10;
            _levels[4].timeStart = 0.8f;
            _levels[4].timeFinish = 0.25f;

            _levels[5]._food = Resources.Load("Food/Cheese", typeof(GameObject)) as GameObject;
            _levels[5]._countEnemies = 60;
            _levels[5]._enemies = new Enemy[3];
            _levels[5]._enemies[0]._enemy = _Cockroach;
            _levels[5]._enemies[0]._frequency = 0;
            _levels[5]._enemies[1]._enemy = _FatCockroach;
            _levels[5]._enemies[1]._frequency = 5;
            _levels[5]._enemies[2]._enemy = _RunnerCockroach;
            _levels[5]._enemies[2]._frequency = 9;
            _levels[5].timeStart = 0.7f;
            _levels[5].timeFinish = 0.25f;


            _levels[6]._food = Resources.Load("Food/Hamburger", typeof(GameObject)) as GameObject;
            _levels[6]._countEnemies = 60;
            _levels[6]._enemies = new Enemy[3];
            _levels[6]._enemies[0]._enemy = _Cockroach;
            _levels[6]._enemies[0]._frequency = 0;
            _levels[6]._enemies[1]._enemy = _FatCockroach;
            _levels[6]._enemies[1]._frequency = 4;
            _levels[6]._enemies[2]._enemy = _RunnerCockroach;
            _levels[6]._enemies[2]._frequency = 8;
            _levels[6].timeStart = 0.6f;
            _levels[6].timeFinish = 0.25f;


            _levels[7]._food = Resources.Load("Food/Hotdog", typeof(GameObject)) as GameObject;
            _levels[7]._countEnemies = 60;
            _levels[7]._enemies = new Enemy[3];
            _levels[7]._enemies[0]._enemy = _Cockroach;
            _levels[7]._enemies[0]._frequency = 0;
            _levels[7]._enemies[1]._enemy = _FatCockroach;
            _levels[7]._enemies[1]._frequency = 4;
            _levels[7]._enemies[2]._enemy = _RunnerCockroach;
            _levels[7]._enemies[2]._frequency = 7;
            _levels[7].timeStart = 0.6f;
            _levels[7].timeFinish = 0.25f;

            _levels[8]._food = Resources.Load("Food/Pie", typeof(GameObject)) as GameObject;
            _levels[8]._countEnemies = 60;
            _levels[8]._enemies = new Enemy[3];
            _levels[8]._enemies[0]._enemy = _Cockroach;
            _levels[8]._enemies[0]._frequency = 0;
            _levels[8]._enemies[1]._enemy = _FatCockroach;
            _levels[8]._enemies[1]._frequency = 4;
            _levels[8]._enemies[2]._enemy = _RunnerCockroach;
            _levels[8]._enemies[2]._frequency = 7;
            _levels[8].timeStart = 0.5f;
            _levels[8].timeFinish = 0.25f;

            _levels[9]._food = Resources.Load("Food/Sausage", typeof(GameObject)) as GameObject;
            _levels[9]._countEnemies = 60;
            _levels[9]._enemies = new Enemy[3];
            _levels[9]._enemies[0]._enemy = _Cockroach;
            _levels[9]._enemies[0]._frequency = 0;
            _levels[9]._enemies[1]._enemy = _FatCockroach;
            _levels[9]._enemies[1]._frequency = 4;
            _levels[9]._enemies[2]._enemy = _RunnerCockroach;
            _levels[9]._enemies[2]._frequency = 7;
            _levels[9].timeStart = 0.5f;
            _levels[9].timeFinish = 0.25f;

            _levels[10]._food = Resources.Load("Food/Apple", typeof(GameObject)) as GameObject;
            _levels[10]._countEnemies = 60;
            _levels[10]._enemies = new Enemy[3];
            _levels[10]._enemies[0]._enemy = _Cockroach;
            _levels[10]._enemies[0]._frequency = 0;
            _levels[10]._enemies[1]._enemy = _FatCockroach;
            _levels[10]._enemies[1]._frequency = 4;
            _levels[10]._enemies[2]._enemy = _RunnerCockroach;
            _levels[10]._enemies[2]._frequency = 6;
            _levels[10].timeStart = 0.5f;
            _levels[10].timeFinish = 0.25f;

            _levels[11]._food = Resources.Load("Food/Bacon", typeof(GameObject)) as GameObject;
            _levels[11]._countEnemies = 60;
            _levels[11]._enemies = new Enemy[3];
            _levels[11]._enemies[0]._enemy = _Cockroach;
            _levels[11]._enemies[0]._frequency = 0;
            _levels[11]._enemies[1]._enemy = _FatCockroach;
            _levels[11]._enemies[1]._frequency = 4;
            _levels[11]._enemies[2]._enemy = _RunnerCockroach;
            _levels[11]._enemies[2]._frequency = 6;
            _levels[11].timeStart = 0.5f;
            _levels[11].timeFinish = 0.25f;

            _levels[12]._food = Resources.Load("Food/Banana", typeof(GameObject)) as GameObject;
            _levels[12]._countEnemies = 60;
            _levels[12]._enemies = new Enemy[3];
            _levels[12]._enemies[0]._enemy = _Cockroach;
            _levels[12]._enemies[0]._frequency = 0;
            _levels[12]._enemies[1]._enemy = _FatCockroach;
            _levels[12]._enemies[1]._frequency = 4;
            _levels[12]._enemies[2]._enemy = _RunnerCockroach;
            _levels[12]._enemies[2]._frequency = 5;
            _levels[12].timeStart = 0.5f;
            _levels[12].timeFinish = 0.25f;

            _levels[13]._food = Resources.Load("Food/Catfood", typeof(GameObject)) as GameObject;
            _levels[13]._countEnemies = 60;
            _levels[13]._enemies = new Enemy[3];
            _levels[13]._enemies[0]._enemy = _Cockroach;
            _levels[13]._enemies[0]._frequency = 0;
            _levels[13]._enemies[1]._enemy = _FatCockroach;
            _levels[13]._enemies[1]._frequency = 4;
            _levels[13]._enemies[2]._enemy = _RunnerCockroach;
            _levels[13]._enemies[2]._frequency = 5;
            _levels[13].timeStart = 0.5f;
            _levels[13].timeFinish = 0.25f;

            _levels[14]._food = Resources.Load("Food/Portalcake", typeof(GameObject)) as GameObject;
            _levels[14]._countEnemies = 60;
            _levels[14]._enemies = new Enemy[3];
            _levels[14]._enemies[0]._enemy = _Cockroach;
            _levels[14]._enemies[0]._frequency = 0;
            _levels[14]._enemies[1]._enemy = _FatCockroach;
            _levels[14]._enemies[1]._frequency = 4;
            _levels[14]._enemies[2]._enemy = _RunnerCockroach;
            _levels[14]._enemies[2]._frequency = 5;
            _levels[14].timeStart = 0.5f;
            _levels[14].timeFinish = 0.25f;




            // Вычисляем время появления каждого таракана на уровне
            // и заполняем массив timeStarts этими значениями
            // думаю нужно попробовать не использовать массив
            //а вычислять это вермя. И посмотреть как это скажеться на производительности
            for (int j = 0; j < _levels.Length; j++)
            {
                /*
                                int tmpCount = 0;

                                for (int i = 0; i < _levels[j]._enemies.Length; i++)
                                {
                                    tmpCount += _levels[j]._enemies[i]._count;
                                }

                                int k = tmpCount / 4;
                                int beginPick = (tmpCount - k) / 2;
                                int endPick = beginPick + k;
                                float deltaTimeBeforePick = (_levels[j].timeBorders -  _levels[j].timePick) / beginPick;
                                float  deltaTimeAfterPick = (_levels[j].timeBorders - _levels[j].timePick)/ (tmpCount - endPick);
                                */

                int tmpCount = _levels[j]._countEnemies;
                _levels[j].timeStarts = new float[tmpCount];
                float step = (_levels[j].timeStart - _levels[j].timeFinish) / tmpCount;
                for (int i = 0; i < tmpCount; i++)
                {
                    _levels[j].timeStarts[i] = _levels[j].timeStart - (step * i);
                }
                /*
                _levels[j].timeStarts[0] = _levels[j].timeBorders;
                for(int i = 1; i<beginPick; i++)
                {
                  _levels[j].timeStarts[i] =   _levels[j].timeStarts[i - 1] - deltaTimeBeforePick;
                }

                for (int i = beginPick; i < endPick; i++)
                {
                    _levels[j].timeStarts[i] = _levels[j].timePick;
                }
                for (int i = endPick; i < tmpCount; i++)
                {
                    _levels[j].timeStarts[i] = _levels[j].timeStarts[i - 1] + deltaTimeAfterPick;
                }
                */
                //int g = 0;
            }

            //_currentLevel = 0;
            //_countWaves = 1;
            //_currentWave = 1;

            //LevelInit(_currentLevel);



            //_respawnPoints = FindObjectsOfType<Respawn>();
            //GUIPauseButton _tmp = (Resources.FindObjectsOfTypeAll<GUIPauseButton>()[0]);
            //_countScore = (Resources.FindObjectsOfTypeAll<GUIScore>())[0];
            //_tmp.ActiveObject = true;
            //_countScore.ActiveObject = true;
            //_textCountEnemies = (Resources.FindObjectsOfTypeAll<GUICountEnemies>())[0];
            //_textCountEnemies.ActiveObject = true;
            //_food = FindObjectOfType<Target>();
            //_loseMenu = Resources.FindObjectsOfTypeAll<GUILoseMenu>()[0];

        }

        // Настраиваем частоту появляния тараканов.
        // массив _levels[currentLevel]._enemies содержит тип и количество тараканов
        // нулевой элемент это тип который появлятся по умолчания, те которых будет больше всего
        // А следующие типы будем высчитывать так сумма предидущего кочества плюс кол-во текущего типа и поделить на кол-во текущего
        // пусть значения массива 20, 5, 6 тогда тараканов первого типа будет 20 второго типа будет (20+5) /5 = 5 тоесть каждый пятый таракан будет второго типа
        // наверное нужно будет вычислить частоту появления и отсортировать массив по убыванию, там посмотрим
        //countEnemies = _levels[currentLevel]._enemies[0]._count;

            /*
        private void Frequency(Enemy[] enemies)
        {

            _countEnemies = 0;
           
            _frequency = new Enemy[enemies.Length];
            for (int i = 0; i < enemies.Length; i++)
            {
                _countEnemies += enemies[i]._count;
                _frequency[i]._enemy = enemies[i]._enemy;
                _frequency[i]._count = _countEnemies / enemies[i]._count;
            }
        }
        */

            // это нужно убрать, не будет волн
        private void WaveInit( int numberLevel)
        {
            _countEnemiesDestroed = 0;
            _currentEnemy = 0;
            _timeInstance = TimeInstaceEnemies(0);
            //Frequency(_levels[numberLevel]._enemies);

        }
        private void LevelInit( int numberLevel) {

            _respawnIndex = Random.Range(0, _respawnPoints.Length);

            _countEnemies = _levels[numberLevel]._countEnemies;
            _countEnemiesDestroed = 0;
            _currentEnemy = 0;
            _timeInstance = TimeInstaceEnemies(0);
            //Frequency(_levels[numberLevel]._enemies);
            _levels[numberLevel]._food.transform.position = new Vector3(0, 0, _levels[numberLevel]._food.transform.position.z);
            _food =  Instantiate(_levels[numberLevel]._food).GetComponent<Target>() ;
            _food.transform.position = new Vector3(_foodPosition.position.x, _foodPosition.position.y,  _food.transform.position.z);
            if (_healthLine)
            {
                _healthLine.MaxValue = _food.CountHits;
                _healthLine.Value = _food.CountHits;
            }

        }
        // определяем тип следующего врага
        private GameObject NextEmeny( int currentEnemy)
        {
            currentEnemy++;

            if (_levels[_currentLevel]._enemies.Length == 1)
            {
                return _levels[_currentLevel]._enemies[0]._enemy;
            }
            for(int i = 1; i < _levels[_currentLevel]._enemies.Length; i++)
            {
                if ( (currentEnemy % _levels[_currentLevel]._enemies[i]._frequency)  == 0)
                {
                    return _levels[_currentLevel]._enemies[i]._enemy;
                }
            }
            return _levels[_currentLevel]._enemies[0]._enemy;
            /*
            if (_frequency.Length > 1)
            {
                for(int i =1; i < _frequency.Length; i++)
                {
                    if ( currentEnemy % _frequency[i]._count == 0)
                    {
                        return _frequency[i]._enemy;
                    }
                }
            }
            return _frequency[0]._enemy; 
            */
            //return null;
        }
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }
            if (Pause) return;

            if (_tmpFlag) return;
            // да это не правильно, но по другому еще хуже
            // это нужно реализовать через событийную модель
            //_healthLine.Value = _food.CountHits;
            if (_food.CountHits <= 0)
            {

                foreach (BaseGUI elem in Resources.FindObjectsOfTypeAll<BaseGUI>())
                {
                    elem.ActiveObject = false;
                }
                //Time.timeScale = 0;
                Pause = true;
                _loseMenu.ActiveObject = true;
                _tmpFlag = true;

            }



            if (!_countScore.ActiveObject)
            {
                _countScore.ActiveObject = true;
                _textCountEnemies.ActiveObject = true;
            }


            if (_currentEnemy >= _countEnemies) return;

             _time = _time + Time.deltaTime;
            if (_time >= _timeInstance)
            {

                /*
                    int i = Random.Range(0, _respawnPoints.Length);
                    //                while ( (i == respP1) || (i== respP2)  )
                    while (i == respP1)
                    {
                        i = Random.Range(0, _respawnPoints.Length);

                    }
                    //                respP2 = respP1;
                    respP1 = i;
                    */
                  _respawnPoints[_respawnIndex].Create(NextEmeny(_currentEnemy));
                _respawnIndex++;
                    if (_respawnIndex >= _respawnPoints.Length)
                {
                    _respawnIndex = 0;
                }
                _time = 0.0f;
                //i = Random.Range(0, _timeInstanceEnemies.Length);
                //Debug.Log(i);
                //_timeInstance = _timeInstanceEnemies[i];
                _timeInstance = TimeInstaceEnemies(_currentEnemy);
                _currentEnemy++;
            }

            _textCountEnemies.CountEnemies(_countEnemies.ToString(), (_countEnemies - _currentEnemy).ToString());
        }
        private float TimeInstaceEnemies()
        {
            int i = Random.Range(0, _timeInstanceEnemies.Length);
            //Debug.Log(i);
            return _timeInstanceEnemies[i];

        }

        private float TimeInstaceEnemies(int current)
        {
            return _levels[_currentLevel].timeStarts[current];
        }
            #endregion
            #region public methods
            public void ScoreUp(int up)
        {

            // не правильно смешивать увеличение очков за уничтожение врага
            // и проверку на прохождение уровня, но пока менять не буду
            _score += up;
            _countScore.ScoreUp(_score.ToString());

            _countEnemiesDestroed++;
            if (_countEnemiesDestroed >= _countEnemies)
            {
                if (_currentWave < _countWaves)
                {
                    _currentWave++;
                    WaveInit(_currentLevel);
                    return;
                }
                _currentWave = 1;
                _currentLevel++;
                if (_currentLevel >= _levels.Length)
                {
                    _currentLevel = 0;
                    _countWaves++;
                }
                
                Object.Destroy(_food.gameObject );
                LevelInit(_currentLevel);
            }

        }
        #endregion
        #region public properties
        public static bool Pause
        {
            get { return _pause; }
            set
            {
                _pause = value;
                if (_pause)
                {
                    Time.timeScale = 0;
                }
                else
                {
                    Time.timeScale = 1;
                }
            }
        }
        public static GameSceneMain Instance
        {
            get { return _instance; }
            private set { _instance = value; }
        }
        #endregion
    }
}
