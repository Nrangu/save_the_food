﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;

namespace SaveTheFood
{

    public class CockroachMoveState : BaseState2D
    {
        #region private fields
        private Vector2 _direction;
        private float _speed;
        private CockroachController _controller;
        #endregion
        #region protected methods
        protected override void UpdateState()
        {
            if (GameSceneMain.Pause) return;
            base.UpdateState();
            Position = Position + ((Vector3)(_direction) * _speed * Time.deltaTime);
        }
        #endregion

        #region public properties
        public Vector2 Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }
        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        public CockroachController Controller
        {
            get { return _controller; }
            set { _controller = value; }
        }
        #endregion
    }
}