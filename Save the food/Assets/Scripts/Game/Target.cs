﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
namespace SaveTheFood
{
    public class Target : BaseSceneObject
    {
        #region private methods
        [SerializeField] private float _countHits = 10;
        private HealthLine _healthLine;

        //        private GUILoseMenu _loseMenu;
        #endregion
        #region private methods
        protected override void Awake()
        {
            base.Awake();
            //          _loseMenu = Resources.FindObjectsOfTypeAll<GUILoseMenu>()[0];
            _healthLine = FindObjectOfType<HealthLine>();
        }
        #endregion
        #region public methods
        public void Hit(float _damage)
        {
            if (_countHits <= 0)
            {
                return;
            }
            _countHits -= _damage;
            if (_healthLine)
            {
                _healthLine.Value = _countHits;
            }

            /*            
                        if ( _countHits <= 0)
                        {
                            foreach (BaseGUI elem in Resources.FindObjectsOfTypeAll<BaseGUI>())
                            {
                                elem.ActiveObject = false;
                            }
                            Time.timeScale = 0;
                            _loseMenu.ActiveObject = true;

                        }
                        */
        }
        #endregion
        #region public properties
        public float CountHits
        {
            get { return _countHits; }
        }
        #endregion

    }
}
