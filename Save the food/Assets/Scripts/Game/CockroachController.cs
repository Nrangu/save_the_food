﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base;
namespace SaveTheFood
{

    public class CockroachController : BaseStatesController
    {
        #region private fields
        [SerializeField] private float _speed = 0;
        [SerializeField] private Sprite _deadImage;
        [SerializeField] private int _score = 1;
        [SerializeField] private int _countHits = 1;
        [SerializeField] private AudioClip _audioClip = null;
        private AudioSource _audioSource = null;
        private Target _target;
        private Vector2 _direction = new Vector2(0, 0);
        #endregion

        #region private methods
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!Enabled) { return; }
            if (!collision.gameObject.GetComponent<Target>()) { return; }

            if (GameSceneMain.Pause) { return; }
            SwitchState(2);
        }

        #endregion

        #region protected methods
        protected override void Awake()
        {
            _countStates = 3;
            _currentState = 0;
            base.Awake();
            _target = FindObjectOfType<Target>();

            _states[0] = InstanceObject.AddComponent<CockroachMoveState>();
            _states[1] = InstanceObject.AddComponent<CockroachDeadState>();
            _states[2] = InstanceObject.AddComponent<CockroachEatState>();

            ((CockroachMoveState)_states[0]).Speed = _speed;
            ((CockroachMoveState)_states[0]).Controller = this;

            ((CockroachDeadState)_states[1]).Image = _deadImage;
            ((CockroachEatState)_states[2]).Target = _target;

            _audioSource = GetComponent<AudioSource>();
            if (!_audioSource)
            {
                _audioSource =  InstanceObject.AddComponent<AudioSource>();
            }
            _audioSource.playOnAwake = false;
            _audioSource.spatialBlend = 0.0f;



        }
        protected void Start()
        {
/*
            _target = FindObjectOfType<Target>();

            if (_target)
            {
                _direction = _target.gameObject.transform.position - transform.position;
            }
            _direction.Normalize();

            _states[0] = InstanceObject.AddComponent<CockroachMoveState>();
            _states[1] = InstanceObject.AddComponent<CockroachDeadState>();
            _states[2] = InstanceObject.AddComponent<CockroachEatState>();

            ((CockroachMoveState)_states[0]).Direction = _direction;
            ((CockroachMoveState)_states[0]).Speed = _speed;
            ((CockroachMoveState)_states[0]).Controller = this;

            ((CockroachDeadState)_states[1]).Image = _deadImage;
            ((CockroachEatState)_states[2]).Target = _target;
            */
            //On();
        }


        protected override void MouseDown()
        {
            if (GameSceneMain.Pause) return;

            base.MouseDown();

            _countHits--;
            if (_countHits > 0) return;
            
            GameSceneMain.Instance.ScoreUp(_score);
            _audioSource.PlayOneShot(_audioClip);
            SwitchState(1);
            Off();
        }
        #endregion
        #region public methods
        public void DirectToTarget()
        {
            int k = 1;
            if (_target)
            {
                _direction = _target.gameObject.transform.position - transform.position;
            }
            _direction.Normalize();
            if (_direction.x > 0)
            {
                k = -1;
            }else
            {
                k = 1;
            }

            ((CockroachMoveState)_states[0]).Direction = _direction;
            transform.Rotate(0, 0, k * Vector2.Angle(Vector2.up, _direction));

        }
        public override void On()
        {
            DirectToTarget();
            ((CockroachMoveState)_states[0]).Direction = _direction;
            base.On();
        }
        #endregion
        #region public properties
        #endregion
    }
}